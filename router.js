var optfile = require('./models/optfile');
var url = require('url');
var querystring = require('querystring');

function getRecall(req,res){
	res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'});
	function recall(data){
		res.write(data);
		res.end('');
	}
	return recall;
}

module.exports = {
	login: function(req,res){
		// var rdata = url.parse(req.url,true).query;
		// console.log(rdata);
		// if(rdata['email']!=undefined){
		// 	console.log(rdata['email']);
		// 	console.log(rdata['pwd']);
		// }
		var post = '';
		req.on('data',function(chunk){
			post += chunk;
		})
		req.on('end',function(){
			post = querystring.parse(post);
			// console.log('email:' + post['email']);
			// console.log('pwd:' + post['pwd']);
			var arr = ['email','pwd'];
			function recall(data){
				dataStr = data.toString();
				for(var i=0;i<arr.length;i++){
					re = new RegExp('{'+ arr[i] +'}','g');
					dataStr = dataStr.replace(re,post[arr[i]]);
				}
				res.write(dataStr);
				res.end('');
			}
			optfile.readFile('./views/login.html',recall);
		})
	},
	regist: function(req,res){
		recall = getRecall(req,res);
		optfile.readFile('./views/regist.html',recall);
	},
	writefile:function(req,res){
        function  recall(data){
            res.write(data);
            res.end('');//不写则没有http协议尾
        }
        optfile.writefile('./views/one.txt','今天阳光灿烂',recall);
    },
	showimg:function(req,res){
        res.writeHead(200,{'Content-Type':'image/jpeg'});
        optfile.readImg('./imgs/1.png',res);
    }
}