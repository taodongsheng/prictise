var fs = require('fs'); 

module.exports = {
	readFile: function(path,recall){
		fs.readFile(path,function(err,data){
			if(err){
				console.log(err);
			}else{
				console.log(data.toString());
				recall(data);
			}
		})
	},
	readImg: function(path,res){
		fs.readFile(path,'binary',function(err,filedata){
			if(err){
				throw err;
				return;
			}else{
				res.write(filedata,'binary');
				res.end();
			}
		})
	},
	writeFile: function(path,data,recall){
		fs.writeFile(path,data,function(err){
			if(err) {
				throw err;
			}
			recall('写文件成功');
		})
	}
}